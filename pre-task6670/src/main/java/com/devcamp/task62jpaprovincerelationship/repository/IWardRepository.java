package com.devcamp.task62jpaprovincerelationship.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task62jpaprovincerelationship.model.Ward;

public interface IWardRepository extends JpaRepository<Ward, Long> {
    Ward findById(int id);
}
