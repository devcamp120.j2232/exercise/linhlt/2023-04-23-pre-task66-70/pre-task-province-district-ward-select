package com.devcamp.task62jpaprovincerelationship.controllers;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task62jpaprovincerelationship.model.District;
import com.devcamp.task62jpaprovincerelationship.model.Ward;
import com.devcamp.task62jpaprovincerelationship.services.DistrictService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class DistrictController {
    @Autowired
    DistrictService districtService;
    //get all provinces lisst
    @GetMapping("/all-districts")
    public ResponseEntity<List<District>> getAllDistricts(){
        try {
            return new ResponseEntity<>(districtService.getAllDistricts(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    //get wards by distrct id
    @GetMapping("/wards")
    public ResponseEntity<Set<Ward>> getWardsByDistrictIdApi(@RequestParam(value = "districtId") int id){
        try {
            Set<Ward> provinceDistricts = districtService.getWardsByDistrictId(id);
            if (provinceDistricts != null ){
                return new ResponseEntity<>(provinceDistricts, HttpStatus.OK);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
